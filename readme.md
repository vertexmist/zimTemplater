
Utility for the zim wiki ( http://zim-wiki.org/ ), to make it easy to initialize new pages depending on the path they are located in.

Create a custom tool and invoke this tool e.g. with:
zimtemplater --root %n --dir %d --output %f --template "template.txt" --startMarker "**TemplateStart**"



usage: zimtemplater [-h] [-r ROOT] [-d DIR] [-o OUTPUT] [--overwrite]
                    [-t TEMPLATE] [-s STARTMARKER] [--version]

Finds a template file, and appends it to the specified output (or to stdout if
no output is specified). If no template is found, nothing is appended.

optional arguments:
  -h, --help                  show this help message and exit

  -r ROOT, --root ROOT        Highest directory level to try to look for
                              templates in. Defaults to /.

  -d DIR, --dir DIR           Directory to start look for templates in (by
                              default use current directory). Looks for
                              templates in parent directories until one is
                              found, or root directory is reached.

  -o OUTPUT, --output OUTPUT  The file to append (or overwrite) the result to.
                              If missing, print to stdout.

  --overwrite                 If present, and the output is being saved to a
                              file with -o, the output will be overwritten
                              instead of appended to.

  -t TEMPLATE,                The filename of the template file to look for.
  --template TEMPLATE         By default template.txt

  -s STARTMARKER,             Start marker for the template. If defined and
  --startMarker STARTMARKER   present, the part of the template file before
                              the start marker is discarded.

  --version                   Print the current version of the program

