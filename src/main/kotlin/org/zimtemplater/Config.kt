package org.zimtemplater

import com.xenomachina.argparser.ArgParser
import com.xenomachina.argparser.InvalidArgumentException
import com.xenomachina.argparser.default
import org.bagofutils.unEscape


/**
 *
 */
class Config(parser: ArgParser) {

    val rootDir by parser.storing("-r", "--root", help = "Highest directory level to try to look for templates in.  Defaults to /.").default("/")
    val dir by parser.storing("-d", "--dir", help = "Directory to start look for templates in (by default use current directory).  Looks for templates in parent directories until one is found, or root directory is reached.").default(".")
    val output by parser.storing("-o", "--output", help = "The file to append (or overwrite) the result to.  If missing, print to stdout.").default(null)
    val overwrite by parser.flagging("--overwrite", help = "If present, and the output is being saved to a file with -o, the output will be overwritten instead of appended to.")
    val templateName by parser.storing("-t", "--template", help = "The filename of the template file to look for.  By default template.txt").default("template.txt")
    val templateStartMarker by parser.storing("-s", "--startMarker", help = "Start marker for the template.  If defined and present, the part of the template file before the start marker is discarded.").default(null)
    val printVersion by parser.flagging("--version", help = "Print the current version of the program")

}