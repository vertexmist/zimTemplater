package org.zimtemplater

import com.xenomachina.argparser.ArgParser
import com.xenomachina.argparser.DefaultHelpFormatter
import com.xenomachina.argparser.mainBody
import java.io.File


private val programName = "zimtemplater"
private val version = "1.0.0"


fun main(args: Array<String>) {
    mainBody(programName) {
        val helpFormatter = DefaultHelpFormatter("Finds a template file, and appends it to the specified output " +
                                                 "(or to stdout if no output is specified).  If no template is found, " +
                                                 "nothing is appended.")
        val parser = ArgParser(args, helpFormatter = helpFormatter)
        val config = Config(parser)

        if (config.printVersion) {
            println("$programName-$version")
            System.exit(0)
        } else {
            try {
                process(config)
            } catch (e: Throwable) {
                error("Error: ${e.message}")
            }
        }
    }
}

fun process(config: Config) {

    val templateFile = findTemplate(config)
    if (templateFile != null) {

        // Read template
        var templateContent = templateFile.readText()

        // Prepare template
        val startMarker = config.templateStartMarker
        if (startMarker != null && templateContent.contains(startMarker)) {
            templateContent = templateContent.substringAfter(startMarker)
        }

        // Output
        if (config.output != null) {
            // Get output file
            val outFile = File(config.output)
            if (!outFile.isFile) throw IllegalArgumentException("The file '$outFile' is not a file")
            if (!outFile.exists()) throw IllegalArgumentException("The file '$outFile' does not exist")

            // Write
            val overwrite = config.overwrite
            if (overwrite) outFile.writeText(templateContent)
            else outFile.appendText(templateContent)
        }
        else {
            // Print to stdout
            println(templateContent)
        }
    }

}

private fun findTemplate(config: Config): File? {
    // Find template file
    var templateFile: File? = null
    val rootDir = File(config.rootDir)
    var currentDir: File? = File(config.dir)

    if (currentDir != null && currentDir != rootDir && (!currentDir.exists() || !currentDir.isDirectory)) {
        // Move up one step if the directory does not exist or is not a dir.
        currentDir = currentDir.parentFile
    }

    while (currentDir != null) {
        // Check current dir
        if (!currentDir.isDirectory) throw IllegalArgumentException("The file '$currentDir' is not a directory")
        if (!currentDir.exists()) throw IllegalArgumentException("The file '$currentDir' does not exist")

        // Search for template file
        templateFile = currentDir.listFiles().find { it.name == config.templateName }
        if (templateFile != null && templateFile.isFile) return templateFile

        // Go to parent dir
        if (currentDir == rootDir) {
            currentDir = null
        } else {
            currentDir = currentDir.parentFile
        }
    }

    // No template found
    return null
}

